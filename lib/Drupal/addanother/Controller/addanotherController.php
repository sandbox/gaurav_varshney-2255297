<?php

/**
 * @file
 * Contains \Drupal\menu\Controller\MenuController.
 */

namespace Drupal\addanother\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Drupal\Core\Access\AccessInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Menu routes.
 */
class addanotherController extends ControllerBase {

  //Takes the user to the node creation page for the type of a given node.
  public function add(NodeInterface $node) {
    return $this->redirect('node.add', array('node_type' => $node->getType()));
  }

  /**
   * Checks access for the subtree controller.
   */
  public function checkaddanotherAccess(Request $request, NodeInterface $node) {
    $config_values = \Drupal::config('addanother.entity.node.' . $node->getType())->get();
    if (!($node->access('create', $this->currentUser()))) {
      return AccessInterface::DENY;
    }
    if (arg(2) == "edit" && !$config_values['addanother_tab_edit']) {
      return AccessInterface::DENY;
    }
    if ($node && $config_values['addanother_tab'] && $this->currentUser()->hasPermission('use add another')) {
      return AccessInterface::ALLOW;
    }
    return AccessInterface::DENY;
  }

}